var CoinapiSDK=require("./lib/coinapi/coinapi_v1")["default"];
var CoinWS=require("./lib/coinapi/coinapi_ws");
var WS_Server=require("./ws_server.js");
require('console-stamp')(console, '[HH:MM:ss.l]');

class KoinServer {
    constructor(){
        this.cfg = require("./srvcfg.json");
        this.coin_sdk = new CoinapiSDK(this.cfg.coinapi_key);
        this.db = require("./lib/db/db.js");
        this.symbols = [];
        this.ws_serv = null;
        this.exchanges = [];
        this.assets = [];
        this.coin_ws = null;
    }

    updateMetadata() {
        console.log("Update metadata", new Date());
        return this.coin_sdk.metadata_list_exchanges()  //exchanges
            .then(exchanges => {
                return this.db.rewriteEntireCollection(this.cfg.db_name, this.cfg.metadata.exchanges_col, exchanges);
            })
            //.then( ()=>db.getEntireCollection(this.cfg.db_name, this.cfg.metadata.exchanges_col) )
            //.then( cursor=>db.toArray(cursor) )
            //.then( data=>console.log(data) )

            .then(() => {
                return this.coin_sdk.metadata_list_assets();
            }) //assets
            .then(assets => {
                return this.db.rewriteEntireCollection(this.cfg.db_name, this.cfg.metadata.assets_col, assets);
            })

            .then(() => {
                return this.coin_sdk.metadata_list_symbols();
            }) //symbols
            .then(symbols => {
                return this.db.rewriteEntireCollection(this.cfg.db_name, this.cfg.metadata.symbols_col, symbols);
            })

            .then(() => console.log("Metadata has been updated", new Date()))
            .catch(err => console.error("Metadata update error:", err));
    }

    initSymbools() {
        var query = {
            exchange_id: {$in: this.cfg.metadata.exchanges},
            asset_id_base: {$in: this.cfg.metadata.bases},
            asset_id_quote: {$in: this.cfg.metadata.quotes},
            symbol_type: 'SPOT'
        };
        return this.db.getData(this.cfg.db_name, this.cfg.metadata.symbols_col, query)
            .then(cursor => this.db.toArray(cursor.project({
                _id: false,
                symbol_id: true,
                exchange_id: true,
                asset_id_base: true,
                asset_id_quote: true
            })))
            .then(data => this.symbols = data);
    }

    initExchanges(){
        return app.db.getData(app.cfg.db_name,app.cfg.metadata.exchanges_col,{exchange_id: {$in: app.cfg.metadata.exchanges} })
            .then( cursor=>app.db.toArray(cursor) )
            .then( data=>this.exchanges=data )
            .then( ()=>{
                var promises=[];
                this.exchanges.forEach( exchange => {
                    promises.push(
                        app.db.getData(app.cfg.db_name,app.cfg.metadata.symbols_col,
                            {
                                exchange_id: exchange.exchange_id,
                                asset_id_base: { $in: app.cfg.metadata.bases },
                                asset_id_quote: { $in: app.cfg.metadata.quotes },
                                symbol_type: "SPOT"
                            })
                        .then( cursor => app.db.toArray(cursor) )
                        .then( data => exchange.symbols=data )
                    );
                });
                return Promise.all(promises);
            })
    }

    initAssets(){
        return app.db.getData(app.cfg.db_name,app.cfg.metadata.assets_col,{ $or: [{ asset_id: {$in: app.cfg.metadata.bases} }, { asset_id: {$in: app.cfg.metadata.quotes} }] } )
            .then( cursor=>app.db.toArray(cursor) )
            .then( data=>this.assets=data );
    }

    checkMetadata() {
        return this.db.getEntireCollection(this.cfg.db_name, this.cfg.metadata.exchanges_col)
            .then(cursor => {
                var count = cursor.count();
                cursor.close();
                return count;
            }).then(count => {
                if (count == 0) throw "No exchanges";
            })
            .then(() => this.db.getEntireCollection(this.cfg.db_name, this.cfg.metadata.assets_col))
            .then(cursor => {
                var count = cursor.count();
                cursor.close();
                return count;
            }).then(count => {
                if (count == 0) throw "No assets";
            })
            .then(() => this.db.getEntireCollection(this.cfg.db_name, this.cfg.metadata.symbols_col))
            .then(cursor => {
                var count = cursor.count();
                cursor.close();
                return count;
            }).then(count => {
                if (count == 0) throw "No symbols";
            })
            .catch(err => {
                console.error("checkMetadata:", err, ". Update metadata immediately");
                return this.updateMetadata();
            });
    }

    start() {
        let me=this;
        console.log("Starting server...");
        me.db.init()
            .then(() => me.checkMetadata())
            .then( () => me.initSymbools() )
            .then( () => me.initExchanges() )
            .then( () => me.initAssets() )
            .then(() => {
                var upd_period = me.cfg.metadata.update_period_h * 60 * 60 * 1000;
                console.log("Metadata update interval:", upd_period, "ms");
                setInterval(me.updateMetadata.bind(me), upd_period)
            }).then( () => {
                me.coin_ws=new CoinWS(me.symbols);
                return me.coin_ws.start();
            }).then(() => {
                me.ws_serv = new WS_Server(me, me.coin_ws.getEmitter());
                return me.ws_serv.init();
            })
            .then(() => {
                const http = require('http'),
                    finalhandler = require('finalhandler'),
                    serveStatic = require('serve-static');

                const serve = serveStatic("../client/web");
                const server = http.createServer(function(req, res) {
                    var done = finalhandler(req, res);
                    serve(req, res, done);
                });
                server.listen(9090, () => {
                    console.log('HTTP listening');
                });
            })
            .catch( (err) => {
                console.error("Server start error, termination");
                if(err) console.error(err);
                process.exit(1);
            } );
    }
}

app = new KoinServer( );
app.start();
