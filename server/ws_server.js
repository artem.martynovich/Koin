var WebSocket = require("ws");
var uuidv1 = require("uuid/v1");
var CoinWS = require("./lib/coinapi/coinapi_ws")

module.exports = class WS_Server {
    constructor(app, emitter){
        this.app = app;
        this.wss = null;
        this.c_mngr = new ClientManager(emitter);
    }

    init() {
        let me=this;
        return new Promise((resolve, reject) => {
                me.wss = new WebSocket.Server({port: me.app.cfg.port, verifyClient: me.verifyClient});
                me.wss ? resolve(me.wss) : reject("WebSocket server creation error");
            }).then(wss => {
                wss.on('connection', socket => {
                    me.c_mngr.add(socket);
                    console.log("Client connected", socket._socket.remoteAddress, me.c_mngr.getId(socket));
                    //console.log(me.c_mngr.listClients());
                    socket.on('message', message => {
                        me.processWsRequest(socket, JSON.parse(message));
                    });
                    socket.on('close', () => {
                        me.c_mngr.remove(socket);
                        console.log("Client disconnected",me.c_mngr.getId(socket));
                        //console.log(me.c_mngr.listClients());
                    });
                });
                return wss;
            }).then (wss => {
                wss.on('error', (err) => {
                    console.log("websocket error:", err.message);
                });
                return wss;
            }).then( () => console.log("Client's websocket connection ready") )
            .catch( err => console.error("WS_Server init error:",err.message) );
    }

    verifyClient(info, next) {
        //todo implement
        next(true);
    }

    processWsRequest(socket, msg) {
        let me=this;
        switch (msg.type) {
            case "subscribe":
                me.c_mngr.subscribe(
                    socket,
                    msg.id,
                    me.getParams(msg)
                );
                console.log(me.c_mngr.listClients(true));
                break;
            case "request":
                let handler=getDataHandler(msg.data_type);
                if(handler) handler(socket,"response", me.getParams(msg));
                break;
            case "unsubscribe":
                me.c_mngr.unsubscribe(socket,msg.id);
                console.log(me.c_mngr.listClients(true));
                break;
            case "command":
                if(msg.data_type === "reload"){
                    let cmd_data=JSON.stringify({ type: "command", data_type: "reload" });
                    for(let client of me.c_mngr.clients.values()){
                        client.sock.send(cmd_data);
                    }
                }
                break;
        }
    }

    getParams(msg){
        var params=Object.assign({},msg);
        return params;
    }
}

//-----------------Client manager--------------

class ClientManager{
    constructor(emitter) {
        this.emitter=emitter;
        this.clients=new Map();
    }

    add(socket) {
        ClientManager.createId(socket);
        this.clients.set(this.getId(socket), {sock:socket, subscribes: new Map()});
    }

    remove(socket){
        let subscribes=this.clients.get(this.getId(socket)).subscribes;
        for(let key of subscribes.keys()) this.unsubscribe(socket, key);    //unsub all
        this.clients.delete(this.getId(socket));
    }
    static createId(socket){ socket.uid=uuidv1( ); }

    getId(socket){ return socket.uid; }

    subscribe(socket, id, params) {
        let client=this.clients.get(this.getId(socket));
        if(client.subscribes.get(params.id)){
            this.unsubscribe(client.sock, params.id);
        }
        let handler=getDataHandler(params.data_type);
        if(handler){
            handler(socket, "subscribe", params) //send 1'st data portion immediately
            let sub = new Subscribe(socket, params, handler, this.emitter);
            sub.init();
            client.subscribes.set(id,sub);
        }
    }

    unsubscribe(socket, id) {
        let client=this.clients.get(this.getId(socket));
        if(client) {
            let subscribe = client.subscribes.get(id);
            if (subscribe) {
                subscribe.deInit();
                client.subscribes.delete(id);
            }
        }
    }

    listClients(print_subscribes) {
        let c_list_str="Clients:";
        let i=1;
        for(let id of this.clients.keys()){
            let socket = this.clients.get(id).sock;
            c_list_str+="\n"+i+".  "+socket._socket.remoteAddress.toString()+" "+id.toString();
            i++;
            if(print_subscribes) {
                c_list_str+="\n\tSubscribes:";
                for (let key of this.clients.get(id).subscribes.keys()) {
                    let params=this.clients.get(id).subscribes.get(key).params;
                    c_list_str+="\n\t"+key+" "+params.data_type;
                }
            }
        }
        return c_list_str+"\n----------";
    }
}

//--------------Subscribe------------------

class Subscribe{
    constructor(socket, params, handler, emitter){
        let me = this;
        me.sock = socket;
        me.params = params;
        me.handler = handler;
        me.emitter = emitter;
        me.event=getEventName(me.params.data_type, me.params.symbol_id);
        me.listener = (data) => this.handler(this.sock, "subscribe", this.params, data);
    }

    init(){
        this.emitter.on(this.event, this.listener)
    }

    deInit(){
        this.emitter.removeListener(this.event, this.listener);
    }
}

//-----------todo rework handlers--------------

function createOptions(sort, window) {
    return {
        sort: sort,
        skip: window ? window.offset : null,
        limit: window ? window.size : null
    };
}

function sendMetadata(socket, msg_type, params) {
    var resp = {
        type: "response",
        id: params.id,
        data_type: "metadata"
    };
    switch(params.metadata_type){
        case "symbols":
            resp.payload = {
                symbols: app.symbols,
                exchanges: app.cfg.metadata.exchanges,
                coins: app.cfg.metadata.bases,
                currencies: app.cfg.metadata.quotes
            }
            break;
        case "assets":
            resp.payload = app.assets;
            break;
        case "exchanges":
            resp.payload = {
                exchanges: app.exchanges,
                assets: app.assets
            }
            break;
    };
    socket.send(JSON.stringify(resp));
}

function roundTimeTo1HRS(date_str){
    var t=new Date(date_str);
    t.setMinutes(0,0,0);
    return t;
}

function sendOHLCVHistory(socket, msg_type, params) {       //todo refactore to class
    var resp = { type: msg_type, id: params.id, data_type: "ohlcv_history", payload: { history_end_time: null, ohlcv_history: [] } };
    if(params.period_id != "1HRS"){
        resp.payload="Unsupported period_id (only 1HRS available)";
        socket.send(JSON.stringify(resp));
        return;
    }
    if( !app.symbols.find( el => (el.symbol_id == params.symbol_id) ) ){
        resp.payload="Unsupported symbol_id: "+params.symbol_id.toString();
        socket.send(JSON.stringify(resp));
        return;
    }

    let col_name=app.cfg.ohlcv_history.collection+"_"+params.symbol_id+"_"+params.period_id;
    let now=new Date;
    now.setSeconds(0);
    now.setMinutes(0);
    now.setMilliseconds(0);
    app.db.getData(app.cfg.db_name, col_name, {}, {sort : [["time_period_start", -1]], limit: 1} )
        .then( cursor => app.db.toArray(cursor) )
        .then( data => {
            if(data.length==0){
                let time_start=new Date(Date.parse("2009-01-01T00:00:00+00:00"));
                let time_end=now;
                time_end.setHours(time_end.getHours()-1); //do not save last period because it hasn't finished
                let count=(Math.abs(time_end - time_start) / 36e5) + 1;
                return app.coin_sdk.ohlcv_historic_data(params.symbol_id, params.period_id, time_start, time_end, count)
                    .then( data => app.db.rewriteEntireCollection(app.cfg.db_name, col_name, data) );       //todo add mark for "ended" history which shouldn't be update anymore
            }
            else{
                if( ((Math.abs(now - data[0].time_period_start) / 36e5)) > 1 ){ //do not save last period because it hasn't finished
                    let time_start=data[0].time_period_start;
                    time_start.setHours(time_start.getHours()+1);
                    let time_end=now;
                    time_end.setHours(time_end.getHours()-1);
                    let count=(Math.abs(time_end - time_start) / 36e5);
                    if(count) {
                        return app.coin_sdk.ohlcv_historic_data(params.symbol_id, params.period_id, time_start, time_end, count)
                            .then(data => {
                                if (data.length) app.db.insertDocuments(app.cfg.db_name, col_name, data);
                            });
                    }
                }
            }
        }).then( () => {
            let query={};
            if(params.time_start){
                if(!query.time_period_start) query.time_period_start={};
                query.time_period_start.$gte=roundTimeTo1HRS(params.time_start);
            }
            if(params.time_end){
                if(!query.time_period_start) query.time_period_start={};
                query.time_period_start.$lte=roundTimeTo1HRS(params.time_end);
            }
            return app.db.getData(app.cfg.db_name, col_name, query)
        }).then( cursor => app.db.toArray(cursor) )
        .then( data => resp.payload.ohlcv_history=data )
        .then( () => app.db.getData(app.cfg.db_name, col_name, {}, {sort : [["time_period_start", -1]], limit: 1} ) )
        .then( cursor => app.db.toArray(cursor) )
        .then( data => resp.payload.history_end_time=data[0].time_period_start )
        .then( () => socket.send(JSON.stringify(resp)) )
        .catch( err=>console.error("sendOHLCVHistory:",err.message) );
}

function sendBook(socket, msg_type, params){
    var collection=app.coin_ws.books_proc.getCollBySymbol(params.symbol_id);
    if(!collection) return;

    if('books_processing' in params == false) params.books_processing=false;
    var options=createOptions(params.sort, params.window);
    options.projection={_id: 0};
    let ask_arr=[];
    let bid_arr=[];
    if(params.books_processing==false) {
        params.books_processing=true;
        app.db.getData(app.cfg.db_name, collection.asks, {}, options)
            .then(cursor => app.db.toArray(cursor))
            .then(data => ask_arr = data)
            .then(() => app.db.getData(app.cfg.db_name, collection.bids, {}, options))
            .then(cursor => app.db.toArray(cursor))
            .then(data => bid_arr = data)
            .then(() => {
                let resp = {
                    type: msg_type,
                    id: params.id,
                    data_type: "book",
                    payload: {asks: ask_arr, bids: bid_arr}
                };
                socket.send(JSON.stringify(resp));
                params.books_processing = false;
            })
            .catch(err => {
                console.error("sendBook error:", err.message);
                params.books_processing=false;
            });
    }
}

function sendTrades(socket, msg_type, params){
    if('sequence' in params == false) params.sequence=0;
    if('trades_processing' in params == false) params.trades_processing=false;

    let collection = app.coin_ws.trades_proc.getCollBySymbol(params.symbol_id);
    var resp = { type: msg_type, id: params.id, data_type: "trades" };
    let options = { projection: {dead_tick: false}, sort: [["sequence", -1]] };
    if(params.limit){
        options.limit=params.limit;
    }
    var query = { sequence: { $gt:params.sequence } };
    if(params.trades_processing==false) {
        params.trades_processing=true;
        app.db.getData(app.cfg.db_name, collection, query, options)
            .then(cursor => app.db.toArray(cursor))
            .then(data => {
                if (data.length){
                    resp.payload = data;
                    params.sequence = data[0].sequence;
                }
                else{
                    resp.payload = [];
                }
                socket.send(JSON.stringify(resp));
                params.trades_processing=false;
            })
            .catch(err => {
                params.trades_processing=false;
                console.error('sendTrades error:', err.message)
            });
    }
}

function parseRTStatsFilter(filter){
    let symbol_ids=[];

    let exchanges = (filter && filter.exchange_id && filter.exchange_id.length) ? new Set(filter.exchange_id) : new Set(app.cfg.metadata.exchanges);
    let bases = (filter && filter.asset_id_base && filter.asset_id_base.length) ? new Set(filter.asset_id_base) : new Set(app.cfg.metadata.bases);
    let quotes = (filter && filter.asset_id_quote && filter.asset_id_quote.length) ? new Set(filter.asset_id_quote) : new Set(app.cfg.metadata.quotes);
    for(var sym of app.symbols){
        if(exchanges.has(sym.exchange_id) && bases.has(sym.asset_id_base) && quotes.has(sym.asset_id_quote)){
            symbol_ids.push(sym.symbol_id);
        }
    }
    return symbol_ids;
}

function sendRTStats(socket, msg_type, params, symbol_id){
    if('send_update' in params == false) params.send_update=false;
    if('symbol_ids' in params == false){
        params.symbol_ids=parseRTStatsFilter(params.filter); //filter backward compatibility
    }
    if('symbol_set' in params == false) params.symbol_set=new Set(params.symbol_ids);

    let collection = app.cfg.rt_stats.collection;
    var resp = { type: msg_type, id: params.id, data_type: "rt_stats", is_snapshot: false};
    let options = { projection: {id: false} };
    if(params.send_update) {
        if( !params.symbol_set.has(symbol_id) ){
            return;
        }
        var query={ symbol_id: symbol_id };
    }
    else{
        if(!params.symbol_ids) return;  //empty symbols
        var query={ symbol_id: { $in: params.symbol_ids } };
        params.send_update=true;
        resp.is_snapshot=true
    }

    app.db.getData(app.cfg.db_name, collection, query, options)
        .then(cursor => app.db.toArray(cursor))
        .then(data => {
            resp.payload = data;
            socket.send(JSON.stringify(resp));
        }).catch( err => console.error('sendRTStats error:', err.message) );
}

var handlers = new Map();
handlers.set("ohlcv_history", sendOHLCVHistory)
    .set("metadata",sendMetadata)
    .set("book", sendBook)
    .set("trades", sendTrades)
    .set("rt_stats", sendRTStats);

var events = new Map();
events.set("trades", "trade_updated")
    .set("book", "book_updated")
    .set("rt_stats", "rt_stats_updated");

function getDataHandler(data_type){
    var handler=handlers.get(data_type);
    if(!handler) console.log("getDataHandler error: Unknnown data type");
    return handler;
}

function getEventName(data_type, symbol_id){
    var event=events.get(data_type);
    if(!event) console.log("getEventName error: Unknnown data type");
    else{
        if(symbol_id) event=event+symbol_id;
    }
    return event;
}
