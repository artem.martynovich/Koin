const WebSocket = require('ws');
cfg = require("./srvcfg.json");

const ws = new WebSocket('ws://'+cfg.host.toString()+':'+cfg.port.toString());

ws.on('open', ()=> {
    req={
        type: 'command',
        data_type: 'reload'
    };
    ws.send(JSON.stringify(req));
    ws.close();
});


