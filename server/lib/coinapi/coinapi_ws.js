const WebSocket = require('ws');
const EventEmitter = require('events');

module.exports = class CoinWS{
    constructor(symbols){
        let me=this;
        me.coin_ws = null;
        me.symbol_map = new Map();

        me.symbols_ids = symbols.map(sym => sym.symbol_id );
        symbols.forEach(sym => {
            var obj=Object.assign({},sym);
            delete obj.symbol_id;
            me.symbol_map.set(sym.symbol_id, obj);
        });
        me.emitter = new EventEmitter();
        me.books_proc = new BooksProcessor(me.symbols_ids,me.emitter);
        me.trades_proc = new TradesProcessor(me.symbol_map,me.emitter);
        me.quotes_proc = new QuotesProcessor(me.symbol_map,me.emitter);
        me.stats_mngr = new CoinStatsManager(me.symbol_map, me.emitter);
        me.watchdog= null;
    }

    start(){
        let me=this;

        return me.books_proc.init()
            .then( () => me.trades_proc.init() )
            .then( () => me.quotes_proc.init() )
            .then( () => me.stats_mngr.init() )
            .then( () => me.connect() )
            .catch( err => console.error("CoinWS start error:",err.message) );
    }

    watchdog_handler(){
        console.log("Coinapi heartbeat watchdog is timed out. Reconnect");
        this.connect();
    }

    connect() {
        let me=this;
        if(!me.watchdog) {
            me.watchdog = setInterval(me.watchdog_handler.bind(me), app.cfg.heartbeat_timeout);
        }

        if(me.coin_ws){
            me.coin_ws.close();
            me.coin_ws=null;
        }

        console.log("Coinapi connecting...");
        return new Promise((resolve, reject) => {
            me.coin_ws = new WebSocket('wss://ws.coinapi.io/v1/');
            me.coin_ws ? resolve(me.coin_ws) : reject("Error opening websocket to coinapi.io");
        }).then(coin_ws => {
            coin_ws.on('open', function open() {
                console.log("Coinapi connected");
                var hello = {
                    "type": "hello",
                    "apikey": app.cfg.coinapi_key,
                    "heartbeat": true,
                    "subscribe_data_type": ["trade", "quote"],
                    "subscribe_filter_symbol_id": me.symbols_ids
                };
                coin_ws.send(JSON.stringify(hello));
            });
            coin_ws.on('message', function incoming(data) {
                clearInterval(me.watchdog);
                me.watchdog = setInterval(me.watchdog_handler.bind(me), app.cfg.heartbeat_timeout); //reset wathcdog on any activity
                data = JSON.parse(data);
                switch (data.type) {
                    case "quote":
                        me.quotes_proc.processIncomingQuotes(data);
                        break;
                    case "trade":
                        me.trades_proc.processIncomingTrades(data);
                        break;
                    case "book":
                        me.books_proc.processIncomingBooks(data);
                        break;
                    case "error":
                        console.log("Coinapi websocket error:", data.message)
                        break;
                }
            });
            coin_ws.on('error', err => {
               console.log("Coinapi websocket error:",err.message);
            });
            coin_ws.on('close', () => {
                console.log("Coinapi websocket closed");
            });
            return coin_ws;
        });
    }

    getEmitter(){
        return this.emitter;
    }
}

class TradesProcessor{
    constructor(symbol_map,emitter){
        let me=this;
        me.emitter=emitter;
        me.clean_tick = 0;
        me.trade_life_len = app.cfg.trades.expire_time_ms / app.cfg.expire_check_period_ms;
        me.symbol_map = symbol_map;
        me.collections = new Map();
        me.sequence = 0;
        me.prices_24h_ago = new Map();
        for(var sym of symbol_map.keys()){
            me.collections.set(sym, "trades_"+sym)
        }
    }

    init(){
        var promises=[];
        for(var col of this.collections.values()){
            promises.push(app.db.eraseCollection(app.cfg.db_name, col));
        }

        return Promise.all(promises)
            .then( () => {
                setInterval(this.cleanOldData.bind(this), app.cfg.expire_check_period_ms);
                setInterval(this.update24hAgoPrices.bind(this), app.cfg.trades["24h_ago_update_interval"]);
            }).catch( err => console.error("TradesProcessor init error:",err.message));
    }

    processIncomingTrades(trade){
        let me=this;
        if(!me.symbol_map.get(trade.symbol_id)) return;
        trade.sequence = me.sequence++;
        trade.time_exchange=new Date(trade.time_exchange);
        trade.time_coinapi=new Date(trade.time_coinapi);
        let collection=me.collections.get(trade.symbol_id);
        trade.exchange_id=me.symbol_map.get(trade.symbol_id).exchange_id;
        trade.asset_id_base=me.symbol_map.get(trade.symbol_id).asset_id_base;
        trade.asset_id_quote=me.symbol_map.get(trade.symbol_id).asset_id_quote;
        trade.dead_tick=me.clean_tick+me.trade_life_len;
        delete trade.type;
        app.db.insertOne(app.cfg.db_name, collection,trade)
            .then( () => {
                me.emitter.emit('trade_updated'+trade.symbol_id);
                me.emitter.emit('trade_update', trade);
            } )

    }

    update24hAgoPrices(){
        let now = new Date();
        now.setDate(now.getDate()-1);
        for(let sym of this.collections.keys()){
            this.get24hAgoPrice(sym, now);
        }
    }

    get24hAgoPrice(symbol_id, now){
        let collection=this.collections.get(symbol_id);
        let pipeline=[
            {
                $project : {
                    time_exchange : 1,
                    price : 1,
                    difference : {
                        $abs : {
                            $subtract : [ now, "$time_exchange" ]
                        }
                    }
                }
            },
            {
                $sort : {difference : 1}
            },
            {
                $limit : 1
            }
        ];

        app.db.aggregate(app.cfg.db_name, collection, pipeline)
            .then( agr_cursor => app.db.toArray(agr_cursor) )
            .then( data => {
                if(data.length){
                    this.prices_24h_ago.set(symbol_id, {price: data[0].price, time_exchange:data[0].time_exchange});
                }
            } )
            .catch( err => console.error("get24hAgoPrice error",err.message) )
    }

    calcNetChg(price_current, symbol_id){
        let resp={net:0, chg:0};
        let price_24h_ago=this.prices_24h_ago.get(symbol_id);
        if(price_24h_ago){
            resp.net=price_current-price_24h_ago.price;
            resp.chg=(price_current-price_24h_ago.price)/price_24h_ago.price;
        }
        return resp;
    }

    cleanOldData(){
        this.clean_tick++;
        var promises=[];
        var query={ dead_tick: { $lte: this.clean_tick } };
        for(let col of this.collections.values()) {
            promises.push(
                app.db.getDocCount(app.cfg.db_name, col)
                    .then( count => {
                        if(count > app.cfg.trades.min_trades_count) {
                            return app.db.deleteDocuments(app.cfg.db_name, col, query);
                        }
                    })
            );
        }
        return Promise.all(promises)
            .catch( err => console.error("cleanOldData error:",err.message) );
    }

    getCollBySymbol(symbol_id){
        return this.collections.get(symbol_id);
    }
}

class QuotesProcessor{
    constructor(symbol_map,emitter){
        let me=this;
        me.emitter=emitter;
        me.symbol_map = symbol_map;
    }

    init(){
        //do nothing at this moment
    }

    processIncomingQuotes(quote){
        let me=this;
        if(!me.symbol_map.get(quote.symbol_id)) return;
        me.emitter.emit('quote_update', quote);
    }
}

class BooksProcessor{
    constructor(symbols_map,emitter){
        let me = this;
        me.symbols_map = new Set(symbols_map);
        me.collections=new Map();
        me.emitter=emitter;
        for(var sym of me.symbols_map.keys()){
            me.collections.set(sym, {asks: "book_asks_"+sym, bids: "book_bids_"+sym} )
        }
    }

    init(){
        let get_book_promises=[]; //todo REMOVE WHEN FULL KEY WILL BE RECEIVED
        var promises=[];
        for(var col of this.collections.values()){
            promises.push(app.db.eraseCollection(app.cfg.db_name, col.asks));
            promises.push(app.db.eraseCollection(app.cfg.db_name, col.bids));
        }
        return Promise.all(promises)
            .then( () => {  //todo REMOVE WHEN FULL KEY WILL BE RECEIVED
                if(app.cfg.books.rest_api_preload) {
                    for (let sym of this.symbols_map.keys()) {
                        let collection = this.collections.get(sym);
                        get_book_promises.push(
                            app.coin_sdk.orderbooks_latest_data(sym, 1)
                                .then(symbols => {
                                    if (symbols.length) {
                                        app.db.rewriteEntireCollection(app.cfg.db_name, collection.asks, symbols[0].asks);
                                        app.db.rewriteEntireCollection(app.cfg.db_name, collection.bids, symbols[0].bids);
                                    }
                                })
                        );
                    }
                }
            }).then( () => Promise.all(get_book_promises) )
            .catch( err => console.error("BooksProcessor init error:",err.message));;
    }

    processIncomingBooks(book){
        let me=this;
        if(!me.symbols_map.get(book.symbol_id)) return;
        let collection=me.collections.get(book.symbol_id);
        if(!collection) return;

        console.log(book);
        var promises=[];
        if(book.is_snapshot){
            promises.push(
                app.db.rewriteEntireCollection(app.cfg.db_name,collection.asks,book.asks)
                    .then( () => app.db.rewriteEntireCollection(app.cfg.db_name,collection.bids,book.bids) )
            );
        }
        else{
            promises.concat( me.updateAskBid(book.asks, collection.asks) );
            promises.concat( me.updateAskBid(book.bids, collection.bids) );
        }
        Promise.all(promises)
            .then( () => {
                me.emitter.emit('book_updated'+book.symbol_id);
                me.emitter.emit('book_update', book)
            }).catch( err => console.error("processIncomingBooks error:",err.message));
    }

    updateAskBid(array, collection){
        var promises=[];
        array.forEach( el => {
            if(el.size) {  //add new
                promises.push(app.db.insertOne(app.cfg.db_name, collection, el))
            }
            else{
                promises.push(app.db.deleteOne(app.cfg.db_name, collection, {price: el.size}))
            }
        });
        return promises;
    }

    getCollBySymbol(symbol_id){
        return this.collections.get(symbol_id);
    }
}

class CoinStatsManager{
    constructor(symbol_map, emitter){
        let me = this;
        me.symbol_map = symbol_map;
        me.emitter = emitter;
        me.uninited_symbols = new Set(symbol_map.keys());
        me.bid_asks = new Map();
    }
    init(){
        let me = this;
        let rt_stats = [];
        for(var sym_id of me.symbol_map.keys()){
            let sym_data=me.symbol_map.get(sym_id);
            let pair_name = sym_data.asset_id_base + " - " + sym_data.asset_id_quote;
            let exchange = sym_data.exchange_id;
            rt_stats.push( {
                symbol_id: sym_id,
                pair_name: pair_name,
                exchange: exchange,
                last_trade: null,
                last_price: null,
                open_price: null,
                close_price: null,
                low_price: null,
                high_price: null,
                net: null,
                change: null,
                volume_base: null,
                volume_quote: null,
                bid_price: null,
                ask_price: null
            });
        }
        app.db.rewriteEntireCollection(app.cfg.db_name, app.cfg.rt_stats.collection, rt_stats)
            .then( () => {
                me.emitter.on('trade_update', trade => {
                    let update={};
                    if(me.uninited_symbols && me.uninited_symbols.has(trade.symbol_id)){        //initially just set values instead nulls
                        update = {
                            $set: {
                                last_trade: trade.time_exchange,
                                last_price: trade.price,
                                open_price: trade.price,
                                close_price: trade.price,
                                low_price: trade.price,
                                high_price: trade.price,
                                volume_base: trade.size,
                                volume_quote: trade.size*trade.price,
                                net: 0,
                                change: 0
                            }
                        }
                    }
                    else{
                        let net_chg=app.coin_ws.trades_proc.calcNetChg(trade.price, trade.symbol_id);
                        update = {
                            $set: {
                                last_trade: trade.time_exchange,
                                last_price: trade.price,
                                close_price: trade.price,
                                net: net_chg.net,
                                change: net_chg.chg
                            },
                            $min: { low_price: trade.price },
                            $max: { high_price: trade.price },
                            $inc: {
                                volume_base: trade.size,
                                volume_quote: trade.size*trade.price
                            }
                        };
                    }
                    app.db.updateDocument(app.cfg.db_name, app.cfg.rt_stats.collection, {symbol_id: trade.symbol_id}, update)
                        .then( () => me.uninited_symbols.delete(trade.symbol_id) )
                        .then( () => me.emitter.emit( 'rt_stats_updated', trade.symbol_id ) );
                });
                me.emitter.on('quote_update', quote => {
                    let previous=me.bid_asks.get(quote.symbol_id);
                    if(  !previous ||  (previous.bid_price != quote.bid_price) || (previous.ask_price != quote.ask_price) ){
                        me.bid_asks.set(quote.symbol_id, { bid_price: quote.bid_price, ask_price: quote.ask_price } )
                        let update = {
                            $set: {
                                bid_price: quote.bid_price,
                                ask_price: quote.ask_price
                            }
                        };
                        app.db.updateDocument(app.cfg.db_name, app.cfg.rt_stats.collection, {symbol_id: quote.symbol_id}, update)
                            .then(() => me.emitter.emit('rt_stats_updated', quote.symbol_id));
                    }
                });
                me.emitter.on('book_update', quote => {
                    //book don't used at this moment
                });
            })
            .catch( err => console.error("CoinStatsManager init error:", err.message) );
    }

}
