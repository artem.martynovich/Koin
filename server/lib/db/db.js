const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const url = 'mongodb://localhost:27017';
var client=null;

function init(){
    return new Promise( (resolve,reject)=>{
        if(!client){
            MongoClient.connect(url, {useNewUrlParser: true})
            .then( (id)=>{
                client=id;
                resolve(client);
            })
            .catch( err=>{
                console.error("MongoDB init error:",err.message);
                reject(err);
            });
        }
        else{
            reject("Already inited");
        }
    });
}

function deInit(){
    return new Promise( (resolve,reject)=>{
        if(client){
            client.close()
                .then( ()=>resolve() )
                .catch( err=>{
                    console.error("deInit error:", err);
                    reject(err);
                });
        }
        else{
            reject("MongoDB wasn't inited");
        }
    });
}

function dropCollection(db_name, col_name){
    return client.db(db_name).collection(col_name).drop()
        .catch( err=>console.error("dropCollection error:",err) );
}

function eraseCollection(db_name, col_name){
    return client.db(db_name).collection(col_name).remove({})
        .catch( err=>console.error("eraseCollection error:",err) );
}

function rewriteEntireCollection(db_name, col_name, data){
    return eraseCollection(db_name, col_name)
        .then( () => {
            if(data.length) {
                return client.db(db_name).collection(col_name).insertMany(data);
            }
        }).catch( err => console.error("rewriteEntireCollection error:", err));
}

function getEntireCollection(db_name, col_name){
    return getData(db_name, col_name, {});
}

function getData(db_name, col_name, query, options){
    return new Promise( (resolve,reject)=>{
            var cursor=client.db(db_name).collection(col_name).find(query, options);
            resolve(cursor);
        })
        .catch( err=>console.error("getData error:",err) );
}

function replaceDocument(db_name, col_name, filter, doc){
    return client.db(db_name).collection(col_name).findOneAndReplace(filter,doc,{upsert:true})
        .catch( err=>console.error("replaceDocument error:",err) );
}

function updateDocument(db_name, col_name, filter, update){
    return client.db(db_name).collection(col_name).updateOne(filter, update, null)
        .catch( err=>console.error("updateDocument error:",err) );
}

function insertOne(db_name, col_name, doc){
    return client.db(db_name).collection(col_name).insert(doc)
        .catch( err=>console.error("insertOne error:",err) );
}

function insertDocuments(db_name, col_name, docs){
    return client.db(db_name).collection(col_name).insertMany(docs)
        .catch( err=>console.error("insertDocuments error:",err) );
}

function deleteDocuments(db_name, col_name, filter){
    return client.db(db_name).collection(col_name).deleteMany(filter)
        .catch( err=>console.error("deleteDocuments error:",err) );
}

function deleteOne(db_name, col_name, filter){
    return client.db(db_name).collection(col_name).deleteOne(filter)
        .catch( err=>console.error("deleteDocuments error:",err) );
}

function toArray(cursor){
    return cursor.toArray()
        .then( data=>{
            cursor.close();
            return data;
        }).catch( err=>console.error("toArray error:",err) )
}

function getDocCount(db_name, col_name){
    return client.db(db_name).collection(col_name).countDocuments()
        .catch( err=>console.error("getDocCount error:",err) );
}

function aggregate(db_name, col_name, pipeline, options){
    return new Promise( (resolve,reject)=> {
        var cursor=client.db(db_name).collection(col_name).aggregate(pipeline, options);
        resolve(cursor);
    }).catch(err => console.error("aggregate error:", err));
}

module.exports = {
    init,
    deInit,
    dropCollection,
    eraseCollection,
    rewriteEntireCollection,
    getEntireCollection,
    getData,
    replaceDocument,
    updateDocument,
    insertOne,
    insertDocuments,
    deleteOne,
    deleteDocuments,
    toArray,
    getDocCount,
    aggregate
};
