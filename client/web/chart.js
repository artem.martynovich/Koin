import {Backend, getChartControl} from "./backend.js";
const chart_control = getChartControl();


var myWidget, backend;
var symbolsMap = new Map(), exchangesMap = new Map();

TradingView.onready(() => {    
    backend = new Backend({
    onMessage: {
        ohlcv_history(msg) {
            myWidget._options.datafeed.onMessage(msg);
        },
        metadata(msg) {
            const exchanges = msg.payload.exchanges;
            for(let ex of exchanges) {
                exchangesMap.set(ex.exchange_id, {
                    name: ex.name,
                    website: ex.website
                });
                for(let e of ex.symbols) {
                    symbolsMap.set(e.symbol_id, {
                        name: e.symbol_id,
                        full_name: e.symbol_id,
                        symbol: e.symbol_id,
                        // base_name?: [string];
                        // ticker?: string;
                        description: `${e.asset_id_base} - ${e.asset_id_quote} @ ${ex.exchange_id}`,
                        type: 'crypto',
                        session: '24x7',
                        exchange: ex.exchange_id,
                        listed_exchange: exchangesMap.get(ex.exchange_id).name,
                        timezone: 'Etc/UTC',
                        pricescale: 100,
                        minmov: 1,
                        has_intraday: true,
                        supported_resolutions: backend.config.RESOLUTIONS,
                        intraday_multipliers: backend.config.RESOLUTIONS,
                    });
                }
            }
            createChart();
        }
    }, 
    onConnect() {
        if(!backend.wasClosed)
            backend.reqMetadata()
        else {
            let [name, from, to] = chart_control.chart.requested_symbol;
            backend.wasClosed = false;
            backend.reqOHLCVHistory(name, from, to);
        }
    }
    });
});

/*
 get: myWidget.symbolInterval()
 set: myWidget.setSymbol('TEST_NAME:TEST_EXCHANGE', '60')
 */

 function createChart() {
    myWidget = window.tvWidget = new TradingView.widget({        
        height: '100%',
        width: '100%',
        toolbar_bg: '#4a4a4a',
        symbol: ((chart_control && chart_control.chart.current_symbol)? chart_control.chart.current_symbol: symbolsMap.keys().next().value),
        interval: backend.config.RESOLUTIONS[0],
        container_id: "tv_chart_container",        
        datafeed: {
            onReady(cb) {                
                
                let exchanges = [];
                for (let [k, v] of exchangesMap) {
                    exchanges.push({value: k, name: v.name, desc: v.name});
                }

                setTimeout(() => {
                    cb({
                        exchanges: [], // TODO: put exchanges here
                        symbols_types: [],
                        supported_resolutions: backend.config.RESOLUTIONS,
                        supports_marks: false,
                        supports_timescale_marks: false,
                        supports_time: false});                    
                }, 0);
            },
            searchSymbols(userInput, exchange, symbolType, onResultReadyCallback) {
                console.debug(userInput, exchange, symbolType);

                let res = [];
                for (let [k, v] of symbolsMap) {
                    if((v.name.search(userInput) != -1) && 
                      (v.name.search('_FTS_')==-1) &&   // TMP: ignore futures
                      (!exchange || exchange==v.exchange))
                        res.push(v);
                }                
                onResultReadyCallback(res);
            },
            resolveSymbol(symbolName, onSymbolResolvedCallback, onResolveErrorCallback) {
                setTimeout(() => onSymbolResolvedCallback(symbolsMap.get(symbolName)), 0);
            },
            onMessage(msg) {
                if(msg.data_type != "ohlcv_history")
                    return;
                if(backend.ohlc_id != msg.id && myWidget.onHistoryCallback) {
                    console.log('dropping old message');
                    return;
                }
                myWidget.messageReceived = true;

                if(!msg.payload.ohlcv_history.length && chart_control.chart.requested_symbol[1] > new Date(msg.payload.history_end_time)) {
                    myWidget.onHistoryCallback([], {nextTime: (new Date(msg.payload.history_end_time)).getTime()/1e3});
                } else {
                    let bars = [];
                    for(let e of msg.payload.ohlcv_history) {
                        bars.push({
                            time: new Date(e.time_period_start).getTime(),
                            open: e.price_open,
                            close: e.price_close,
                            high: e.price_high,
                            low: e.price_low,
                            volume: e.volume_traded
                        });                                
                    }
                    if(!bars.length)
                        myWidget.onHistoryCallback(bars, {noData: true});
                    else
                        myWidget.onHistoryCallback(bars, {});
                }
            },
            getBars(symbolInfo, resolution, from, to, onHistoryCallback, onErrorCallback, firstDataRequest) {
                console.debug(symbolInfo, resolution, from, to, firstDataRequest);

                myWidget.onHistoryCallback = onHistoryCallback;
                backend.ohlc_id = backend.newid;

                let now = new Date(),
                    date_from = new Date(from*1000),
                    date_to = new Date(to*1000);
                chart_control.chart.requested_symbol = [symbolInfo.name, date_from, date_to];
                if(backend.connected) {
                    backend.reqOHLCVHistory(symbolInfo.name, date_from, date_to);
                }
                
            },
            subscribeBars(symbolInfo, resolution, onRealtimeCallback, subscriberUID, onResetCacheNeededCallback) {
//                 onRealtimeCallback(); //bar: object {time, close, open, high, low, volume}
            },
            unsubscribeBars(subscriberUID) {

            }
        },
        library_path: "charting_library/",
        //  Regression Trend-related functionality is not implemented yet, so it's hidden for a while
        // drawings_access: { type: 'black', tools: [ { name: "Regression Trend" } ] },
        disabled_features: ["use_localstorage_for_settings", "left_toolbar"]
    });        

    myWidget.onChartReady(() => {
        let chart = window.tvWidget.chart();
        chart.onSymbolChanged().subscribe(null, (e) => {
                console.log(e);
                if(chart_control) {
                    let symbol_id = e.symbol;
                    chart_control.chart.current_symbol = symbol_id;
                    chart_control.ask_bid_trade.display(symbol_id);
                }
                if(!myWidget.messageReceived) {
                    console.log('cancelling ', chart_control.chart.requested_symbol,  ' in favor of ', e.symbol);
                    myWidget.onHistoryCallback([], {noData: true});
                    backend.wasClosed = true;
                    backend.close();
                }
                myWidget.messageReceived = false;
        });
        if(chart_control.chart.current_symbol && chart_control.chart.current_symbol != myWidget.symbolInterval().symbol) {
            myWidget.setSymbol(chart_control.chart.current_symbol, '60');
        }
    });
 }

if(chart_control) {
    chart_control.chart.display = (symbol_id, cb) => {
        console.debug(symbol_id);

        if(myWidget && myWidget._ready)
            myWidget.setSymbol(symbol_id, '60', () => cb());
        else {
            console.log(symbol_id);
            chart_control.chart.current_symbol = symbol_id;
        }
    }
}
