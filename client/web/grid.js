import {Backend, getChartControl} from "./backend.js";
const chart_control = getChartControl();


const floatFormatter = new Intl.NumberFormat("default", {
    style: 'decimal', 
    minimumFractionDigits: 4,
    maximumFractionDigits: 4
});
const percentFormatter = new Intl.NumberFormat("default", {
    style: 'decimal', 
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});

class ColumnRenderer {
    constructor(field, render) {
        this._field = field;
        this._render = render;
    }
    text(r) {
        return this._render(r);
    }
    HTML(r) {
        return `<div id="${this._field}:${r.symbol_id}">
                    ${this._render(r)}
                </div>`
    }
}
const renderers = {
    'last_trade': new ColumnRenderer('last_trade', (r) => 
        r.last_trade==null? 'n/a': (new Date(r.last_trade)).toLocaleTimeString()
    ),
    'last_price': new ColumnRenderer('last_price', (r) => 
        r.last_price==null? 'n/a': percentFormatter.format(r.last_price)
    ),
    'bid_price': new ColumnRenderer('bid_price', (r) => 
        r.bid_price==null? 'n/a': percentFormatter.format(r.bid_price)
    ),
    'ask_price': new ColumnRenderer('ask_price', (r) => 
        r.ask_price==null? 'n/a': percentFormatter.format(r.ask_price)
    ),
    'change': new ColumnRenderer('change', (r) => {
        let color = 'default', sign = '', val = r.change*100;
        if(val > 0) {
            color = 'green';
            sign = '+';
        } else if(val < 0) {
            color = 'red';
            sign = '-';
        }       
        val = percentFormatter.format(Math.abs(val));

        return `<div class=koin-change-${color}>
                    ${sign} ${val} %
                </div>`;
    }),
    'volume_quote': new ColumnRenderer('volume_quote', (r) => 
        r.volume_quote==null? 'n/a': percentFormatter.format(r.volume_quote)
    )
};

const koinGrid = $('#koinGrid').w2grid({ 
    name   : 'koinGrid',
    show : {
        toolbar : true,
        toolbarReload: false,
        toolbarSearch: false,
        searchAll: false,
        toolbarInput: false
    },
    toolbar: {
        items: [{
            type: 'menu-radio', id: 'menuView', text: function(e) {
                 return e.selected.length? this.get('menuView:'+e.selected).text: '';
            },
            items: [{
                id: 'PAIR', text: 'View By Pair'
            }, {
                id: 'EXCHANGE', text: 'View By Exchange'
            }],
            selected: 'PAIR'
        }, {   
            type: 'break' 
        }, { 
            type: 'menu-radio', id: 'menuExchange', text: 'Exchange',
            selected: []
          }, {
            type: 'menu-radio', id: 'menuBase', text: 'Base Currency',
            selected: []
          }, {
            type: 'menu-radio', id: 'menuQuote', text: 'Quote Currency',
            selected: []
          }
        ],
        onRefresh(event) {
            if(['menuExchange', 'menuBase', 'menuQuote'].includes(event.target)) {
                let g = this.owner, mode = g.toolbar.get('menuView').selected;
                g.getFilter(mode);
            } else if(event.target == 'menuView') {
                let g = this.owner, mode = g.toolbar.get('menuView').selected;
                if(mode == 'PAIR')
                    this.hide('menuExchange');
                else
                    this.show('menuExchange');
                g.getFilter(mode);
            }
        }
    },
    columns: [
        { 
            field: 'last_trade', 
            caption: 'Time', 
            sortable: true, 
            size: '5', 
            render: (r) => renderers.last_trade.HTML(r)
        }, {
            field: 'pair_name', 
            caption: 'Symbol', 
            sortable: true, 
            size: '6'
        }, { 
            field: 'symbol_id', 
            caption: 'Pair Name', 
            sortable: true, 
            size: '10%',
            render: (r) => {
                let s = symbolsMap.get(r.symbol_id), 
                    b = assetMap.get(s.base).name,
                    q = assetMap.get(s.quote).name;
                return `${b} - ${q}`;
            }
        }, { 
            field: 'exchange', 
            caption: 'Exchange', 
            sortable: true, 
            size: '10%'
        }, { 
            field: 'last_price', 
            caption: 'Last Price', 
            sortable: true, 
            size: '10%', 
            render: (r) => renderers.last_price.HTML(r)
        }, { 
            field: 'bid_price', 
            caption: 'Bid Price', 
            sortable: true, 
            size: '10%', 
            render: (r) => renderers.bid_price.HTML(r)
        }, { 
            field: 'ask_price', 
            caption: 'Ask Price', 
            sortable: true, 
            size: '10%', 
            render: (r) => renderers.ask_price.HTML(r)
        }, { 
            field: 'change', 
            caption: 'Change (24h)', 
            sortable: true, 
            size: '7', 
            render: (r) => renderers.change.HTML(r)
        }, { 
            field: 'volume_quote', 
            caption: 'Volume (24h)', 
            sortable: true,
            size: '10%', 
            render: (r) => renderers.volume_quote.HTML(r)
        }
    ],

    onSelect(event) {        
        var me = this, id = event.recid;
        chart_control.window.show();        
        
        chart_control.ask_bid_trade.display(id);        
        chart_control.chart.display(id, () => {
            // console.log('chart was loaded');
        });
        // console.log(event, this.get(id));
    },

    setExchanges() {
        const me = this;

        const   menuExchange = me.toolbar.get('menuExchange'),
                menuBase = me.toolbar.get('menuBase'),
                menuQuote = me.toolbar.get('menuQuote');
        me.menuExchange = menuExchange;
        me.menuBase = menuBase;
        me.menuQuote = menuQuote;
        me.menuView = me.toolbar.get('menuView');

        let bases = new Set(), quotes = new Set();

        menuExchange.items = [{id: 'ALL', text: 'ALL'}, { text: '--' }];
        menuExchange.selected = [];
        for (let [k, v] of exchangesMap) {
            let n = 0;
            v.symbols.forEach((e) => {
                bases.add(e.base);
                quotes.add(e.quote);
                n++;
            });

            if(n > 0)
                menuExchange.items.push({
                    id: k,
                    text: v.name
                });
        }
        me.bases = bases;
        me.quotes = quotes;
        menuExchange.selected = 'ALL';

        menuBase.items = [{id: 'ALL', text: 'ALL'}, { text: '--' }];
        menuBase.selected = [];
        for (let v of bases) {
            menuBase.items.push({
                id: v,
                text: assetMap.get(v).name
            });
        }
        menuBase.selected = 'ALL';

        menuQuote.items = [{id: 'ALL', text: 'ALL'}, { text: '--' }];
        menuQuote.selected = [];
        for (let v of quotes) {
            menuQuote.items.push({
                id: v,
                text: assetMap.get(v).name
            });
        }
        menuQuote.selected = 'ALL';

        me.toolbar.refresh();
    },

    getFilter(mode) {
        const me = this;
        if(!me.menuExchange || !me.menuBase || !me.menuQuote)
            return;
        if(me.selectedExchange == me.menuExchange.selected &&
            me.selectedQuote == me.menuQuote.selected &&
            me.selectedBase == me.menuBase.selected)
            return;
        me.selectedExchange = me.menuExchange.selected;
        me.selectedQuote = me.menuQuote.selected;
        me.selectedBase = me.menuBase.selected;

        let filter = {
            exchanges: (me.menuExchange.selected=='ALL' || mode=='PAIR')? exchangesMap.keys(): [me.menuExchange.selected],
            bases: me.menuBase.selected=='ALL'? me.bases: [me.menuBase.selected],
            quotes: me.menuQuote.selected=='ALL'? me.quotes: [me.menuQuote.selected]
        };
        let symbols = [], filteredBases = new Set, filteredQuotes = new Set, filteredExchanges = new Set;
        for(let e of filter.exchanges) {
            for(let b of filter.bases) {
                for(let q of filter.quotes) {
                    let symbol = `${e}_SPOT_${b}_${q}`;
                    if(symbolsMap.has(symbol)) {
                        symbols.push(symbol);
                        filteredExchanges.add(e);
                        filteredBases.add(b);
                        filteredQuotes.add(q);
                    }
                }
            }
        }
        
        symbols.sort();

        let equal = true;
        if(!me.symbols || me.symbols.length != symbols.length) {
            equal = false;
        } else {
            for(let i in symbols) {
                if(me.symbols[i] != symbols[i]) {
                    equal = false;
                    break;
                }
            }
        }
        if(!equal) {
            me.symbols = symbols;
            be.symbol_ids = symbols;
        } else {
            me.menuBase.count = me.menuBase.selected;
            me.menuQuote.count = me.menuQuote.selected;
            me.menuExchange.count = me.menuExchange.selected;
            return;
        }

        console.log(symbols);
//         be.filter = filter;
        me.clear();
        me.lock('Loading...', true);
        be.unsubscribe('rt_stats', be.id);
        be.subid = be.newid;
        be.symbol_ids = symbols;

        let bases = new Set(), quotes = new Set();
        let exchanges = [me.menuExchange.selected];
        if(me.menuExchange.selected == 'ALL')
            exchanges = exchangesMap.keys();
        for(let e of exchanges) {
            let exchange = exchangesMap.get(e);
            console.debug(exchange);
            for(let s of exchange.symbols) {
                bases.add(s.base);
                quotes.add(s.quote);
            }
        }
        console.log(bases, quotes, filteredExchanges, filteredBases, filteredQuotes);

        me.menuBase.items = [{id: 'ALL', text: 'ALL'}, { text: '--' }];
        me.menuBase.count = 0;
        for (let v of filteredBases) {
            me.menuBase.items.push({
                id: v,
                text: assetMap.get(v).name
            });            
        }
        if(!bases.has(me.menuBase.selected)) {
            me.menuBase.selected = 'ALL';
        }


        me.menuQuote.items = [{id: 'ALL', text: 'ALL'}, { text: '--' }];
        me.menuQuote.count = 0;
        for (let v of filteredQuotes) {
            me.menuQuote.items.push({
                id: v,
                text: assetMap.get(v).name
            });            
        }
        if(!quotes.has(me.menuQuote.selected)) {
            me.menuQuote.selected = 'ALL';
        }
        
        me.menuBase.count = me.menuBase.selected;
        me.menuQuote.count = me.menuQuote.selected;
        me.menuExchange.count = me.menuExchange.selected;
    },

    refreshLater(interval_ms) {
        const me = this;
        if(me.refreshTimeout)
            return;
        me.refreshTimeout = setTimeout(() => {
            if([me.menuExchange, me.menuBase, me.menuQuote, me.menuView].find(e => e.checked) != undefined) {
                console.log('later');
                me.refreshTimeout = null;
                return me.refreshLater(interval_ms);
            }
            let sd = me.sortData[0];
            if(sd) {
                console.log('refresh');
                w2ui.koinGrid.localSort(false, true);
                w2ui.koinGrid.refresh();
            }
            me.refreshTimeout = null;
        }, interval_ms);
    },

    updateLater(records, interval_ms) {
        const me = this;
        if(me.updateTimeout)
            return;
        if(!me.uniqRecords)
            me.uniqRecords = new Map;
        for(let r of records)
            me.uniqRecords.set(r.recid, r);
        me.updateTimeout = setTimeout(() => {
            for(let e of me.uniqRecords.values()) {
                me.set(e.recid, e, true);
                for(let c of me.columns) {
                    if(!e.hasOwnProperty(c.field))
                        continue;
                    let el = document.getElementById(`${c.field}:${e.symbol_id}`);
                    if(!(el && renderers[c.field]))
                        continue;

                    let prevVal = undefined;
                    if(me.sortData && me.sortData[0] && me.sortData[0].field == c.field)
                        prevVal = el.innerHTML;
                    el.innerHTML = renderers[c.field].text(e);
                    if(prevVal != undefined && prevVal.trim() != el.innerHTML.trim()) {
                        // console.debug('refresh needed');
                        me.refreshLater(be.config.GRID_REFRESH);
                    }
                }
            }
            me.uniqRecords.clear();
            me.updateTimeout = null;
        }, interval_ms);
    }
});


const   exchangesMap = new Map(), 
        symbolsMap = new Map(), 
        assetMap = new Map();

const be = new Backend({
onMessage: {
    reload(d) {
        location.reload();
    },
    metadata(d) {
        console.log(d.payload);
        const exchanges = d.payload.exchanges,
              assets = d.payload.assets;
        for(let e of exchanges) {
            let syms = [];
            for(let s of e.symbols) {
                symbolsMap.set(s.symbol_id, {
                    exchange: s.exchange_id,
                    base: s.asset_id_base,
                    quote: s.asset_id_quote
                });
                syms.push({
                    base: s.asset_id_base,
                    quote: s.asset_id_quote
                });
            }
            exchangesMap.set(e.exchange_id, {
                name: e.name,
                website: e.website,
                symbols: syms
            });
        }
        for(let e of assets) {
            assetMap.set(e.asset_id, {
                name: e.name,
                is_crypto: e.type_is_crypto
            });
        }
        koinGrid.setExchanges();
    },
    rt_stats(d) {
        if(be.subid && be.subid != be.id) {
            console.log('dropping old message');
            return;
        }
        const records = d.payload;
        for(let e of records) { 
            e.recid = e.symbol_id 
        }
        if(d.is_snapshot) {
            koinGrid.unlock();
            koinGrid.clear(true);
            koinGrid.add(records);
        } else {
            koinGrid.updateLater(records, be.config.GRID_UPDATE);
        }
    }
}, 
onConnect() {
    if(be.wasDisconnected) {
        location.reload();
        return;
    }
    be.reqMetadata();
    be.reqAllStats();
    koinGrid.unlock();    
}, 
onDisconnect() {
    koinGrid.lock('Connecting...', true);
    be.wasDisconnected = true;
}});

