function get_uuid(){ //uses Google Chrome's Crypto instead of plain Math.random
  var random_string    = Array.prototype.map.call(crypto.getRandomValues(new Uint32Array(20)), function(n){return n.toString(16)}).join("")
     ,random_y_bit     = [8, 9, 'a', 'b'][crypto.getRandomValues(new Uint8Array(1))[0] % 4]
     ,template_uuid    = /.*(........)..(....)..(...)..(...)..(............).*/
     ,replace_pattern  = "$1-$2-4$3-" + random_y_bit + "$4-$5"
     ;
  return random_string.replace(template_uuid, replace_pattern);
}

export async function fetchConfig (main) {
  if(typeof require == 'undefined') {
      let response = await fetch("config.json");
      let data = await response.json();
      let cfg = data;
      main(cfg);
  } else {
      const cfg = require('./web/config.json');
      main(cfg);
  }
}

export function getChartControl() {
    if (typeof require !== 'undefined') {
        return require('./chart_control');
    }
}

export class Backend {
    constructor(cb) {
        this._sub_id = get_uuid();
        let me = this;

        this._filter = null;
        this._sort = null;
        this._limit = null;
        this._symbol_ids = null;
        this._cb = cb;

        this._reconnectTimer = null;
        this._connected = false;
        fetchConfig(cfg => {
            this.config = cfg;
            this._endpoint = cfg.endpoints[cfg.ENDPOINT].WS_ENDPOINT;
            this._reconnect();
        });
    }

    get newid() {
        this._sub_id = get_uuid();
        return this._sub_id;
    }

    get id() {
        return this._sub_id;
    }

     reqOHLCVHistory(symbol_id, from, to) {
        this._ws.send(JSON.stringify({
            type: "request",
            id: this._sub_id,
            data_type: "ohlcv_history",
            symbol_id: symbol_id,
            period_id: "1HRS", //only 1HRS available
            time_start: from,
            time_end: to,
            sort: null,//[["price_low", 1]], //nullable 1 for ascending, -1 for descending
            window: null//{ offset:0, size:1000 } //null to receive all data
        }));
    }

    _reconnect() {
        var me = this;
        this._reconnectTimer = null;
        this._ws = new WebSocket(this._endpoint);
        this._ws.onclose = this._ws.onerror = (e) => {
            this._connected = false;
//             console.log(e);
            if(!this._firstDisconnect) {
                this._firstDisconnect = true;
                this._cb.onDisconnect && this._cb.onDisconnect();
            }
            if(!this._reconnectTimer) {
                let tm = 1000;
                if(this._wasClosed) {
                    this._wasClosed = false;
                    tm = 0;
                }
                this._reconnectTimer = setTimeout(() => this._reconnect(), tm);
            }
        }
        this._ws.onmessage = (msg) => {
            const d = JSON.parse(msg.data);
            me._cb.onMessage[d.data_type] && me._cb.onMessage[d.data_type](d);
        };
        this._ws.onopen = () => {
            this._connected = true;
            this._subscription = null;
            this._firstDisconnect = false;
            this._cb.onConnect && this._cb.onConnect();
        };
    }

    get connected() {
        return this._connected;
    }

    set filter(newfilter) {
        this._filter = newfilter;
        this.reqAllStats();
        this.subscribeStats();
    }

    set symbol_ids(ids) {
        this._symbol_ids = ids;
        this.reqAllStats();
        this.subscribeStats();
    }

    set sort(newsort) {
        this._sort = newsort;
        this.reqAllStats();
        this.subscribeStats();
    }

    set limit(newlimit) {
        this._limit = newlimit;
        this.reqAllStats();
        this.subscribeStats();
    }

    reqAllStats() {
        this._ws.send(JSON.stringify({
            type: "request",
            id: this._sub_id,
            data_type: "rt_stats",
//             filter: this._filter,
            symbol_ids: this._symbol_ids,
            sort: this._sort,
            window: this._limit
        }));
    }

    subscribeStats() {        
        this._ws.send(JSON.stringify({
            type: "subscribe",
            id: this._sub_id,
            data_type: "rt_stats",
//             filter: this._filter,
            symbol_ids: this._symbol_ids,
            sort: this._sort,
            window: this._limit
        }));
    }

    subscribe(data_type, symbol_id, limit) {
        this._ws.send(JSON.stringify({
            type: "subscribe",
            id: this._sub_id,
            data_type: data_type,
            limit: limit,
            symbol_id: symbol_id
        }));
    }

    reqMetadata() {
        this._ws.send(JSON.stringify({
            type: "request",
            data_type: "metadata",
            metadata_type: "exchanges"
        }));        
    }

    unsubscribe(data_type, id) {
        this._ws.send(JSON.stringify({
            type: "unsubscribe",
            id: (id? id: this._subscription),
            data_type: data_type
        }));
        this._subscription = null;
    }

    close() {
        this._wasClosed = true;
        this._connected = false;
        this._ws.close();
    }
}