import {Backend, getChartControl} from "./backend.js";
const chart_control = getChartControl();


$('#orderbook_bid').w2grid({ 
    name: 'orderbook_bid', 
    columns: [                
        { field: 'price', caption: 'Bid Price', size: '30%', render: 'float:4' },
        { field: 'size', caption: 'Amount', size: '30%', render: 'float:4' },
        { field: 'value', caption: 'Value', size: '40%', render: 'float:4' }
    ],
    records: [],
});

$('#orderbook_ask').w2grid({ 
    name: 'orderbook_ask', 
    columns: [                
        { field: 'price', caption: 'Ask Price', size: '30%', render: 'float:4' },
        { field: 'size', caption: 'Amount', size: '30%', render: 'float:4' },
        { field: 'value', caption: 'Value', size: '40%', render: 'float:4' }
    ],
    records: [],
});

$('#trades').w2grid({ 
    name: 'trades', 
    header: 'Trades',
    show: { header: true },
    columns: [                
        { field: 'time', caption: 'Time', size: '40%', sortable: true, render: (r) => (new Date(r.time)).toLocaleTimeString() },
        { field: 'price', caption: 'Price', size: '30%', sortable: true, render: 'float:2' },
        { field: 'size', caption: 'Size', size: '30%', sortable: true },
    ],
    records: [],
    recid: 'recid',
    sortData: [ 
        {field: 'time', direction: 'desc'}
    ]
});


const formatter = new Intl.NumberFormat("default", {
    style: 'decimal', 
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});


const backend = new Backend({
onMessage: {
    reload(d) {
        location.reload();
    },
    book(msg) {
        if(msg.id != backend.book_id) {
            console.log(`dropping ${msg.data_type} message: current=${backend.book_id} msg=${msg.id}`);
            return;
        }

        w2ui.orderbook_bid.unlock();
        w2ui.orderbook_ask.unlock();
    
        w2ui.orderbook_ask.records = msg.payload.asks;
        for(let [i,e] of w2ui.orderbook_ask.records.entries()) {
            e.value = e.price*e.size;
            e.recid = i;
        }
        console.debug(w2ui.orderbook_ask.refresh());

        w2ui.orderbook_bid.records = msg.payload.bids
        for(let [i,e] of w2ui.orderbook_bid.records.entries()) {
            e.value = e.price*e.size;
            e.recid = i;
        }
        console.debug(w2ui.orderbook_bid.refresh());
    },
    trades(msg) {
        if(msg.id != backend.trades_id) {
            console.log(`dropping ${msg.data_type} message: current=${backend.trades_id} msg=${msg.id}`);
            return;
        }

        w2ui.trades.unlock();

        let records = [], uuids = new Map;
        for(let e of msg.payload) {
            e.recid = e.uuid;
            e.time = e.time_exchange;
            let r = w2ui.trades.get(e.recid);
            if(r) {
                if(r.sequence < e.sequence) {
                    w2ui.trades.set(e.recid, e, true);
                    w2ui.trades.refreshRow(e.recid);
                }
            } else {
                if(uuids.get(e.uuid) && e.sequence <= uuids.get(e.uuid).sequence) {
                    console.log('duplicate');
                } else
                    uuids.set(e.uuid, e);
            }
        }
        if(uuids.size)
            w2ui.trades.add([...uuids.values()]);
    },
    rt_stats(msg) {
        if(msg.id != backend.stats_id) {
            console.log(`dropping ${msg.data_type} message: current=${backend.stats_id} msg=${msg.id}`);
            return;
        }
        for(let c of [  'ask_price', 'bid_price', 'change', 'net', 
                        'high_price', 'low_price', 'last_price',
                        'volume_base', 'volume_quote']) {
            let e = document.getElementById('koin-stat-'+c);
            if(!e) {
                console.error("Field ${c} missing");
            }
            let r = msg.payload[0];
            if(!r) {
                console.error("No records received");
                return;
            }
            if(c=='change') {
                let color = 'default', sign = '', val = r.change*100;
                if(val > 0) {
                    color = 'green';
                    sign = '+';
                } else if(val < 0) {
                    color = 'red';
                    sign = '-';
                }       
                val = formatter.format(Math.abs(val));

                e.textContent = `${sign} ${val}`;
                e.classList.toggle(`koin-change-default`, false);
                e.classList.toggle(`koin-change-green`, false);
                e.classList.toggle(`koin-change-red`, false);
                e.classList.toggle(`koin-change-${color}`, true);
            } else {
                e.textContent = formatter.format(r[c]);
            }
        }
    }
}, 
onConnect() {
    if(backend.wasDisconnected && !backend.wasClosed) {
        location.reload();
        return;
    }    
    if(chart_control) {
        chart_control.ask_bid_trade.display = function(sym) {
            if(!sym && !chart_control.ask_bid_trade.current_symbol)
                return;
            if(sym && chart_control.ask_bid_trade.current_symbol == sym) {
                console.log('same symbol')
                return;
            }

            if(!backend.wasClosed && sym) {
                chart_control.ask_bid_trade.current_symbol = sym;
                backend.wasClosed = true;
                backend.close();
                return;
            } else {
                backend.wasClosed = false;
            }
            
            if(!sym)
                sym = chart_control.ask_bid_trade.current_symbol;
            console.log(`${chart_control.ask_bid_trade.current_symbol} -> ${sym}`)            
            
            if(backend.book_id)
                backend.unsubscribe('book', backend.book_id);
            if(backend.trades_id)
                backend.unsubscribe('trades', backend.trades_id);
            if(backend.stats_id)
                backend.unsubscribe('rt_stats', backend.stats_id);
            backend.newid;
            w2ui.orderbook_bid.clear();
            w2ui.orderbook_ask.clear();
            w2ui.trades.clear();

            backend.book_id = backend.newid;
            backend.subscribe('book', sym);
            backend.trades_id = backend.newid;
            backend.subscribe('trades', sym, backend.config.TRADES_LIMIT);
            backend.stats_id = backend.newid;
            backend.symbol_ids = [sym];
            chart_control.ask_bid_trade.current_symbol = sym;
        }

        chart_control.ask_bid_trade.display();
    }    
}, 
onDisconnect() {
    backend.wasDisconnected = true;
    w2ui.orderbook_bid.lock('loading...', true);
    w2ui.orderbook_ask.lock('loading...', true);
    w2ui.trades.lock('loading...', true);
}});
