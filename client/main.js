process.on('uncaughtException', function(error) {
  var message = '[' + (new Date()).toISOString() + '] ' + 
                error.stack + '\n' + 
                Array(81).join('-') + '\n\n';

  require('fs').appendFileSync('error.log', message);

  // process.exit(1);
});


const chart_control = require('./chart_control'),
	cfg = require('./web/config.json'),
    addr = cfg.endpoints[cfg.ENDPOINT].HTTP_ENDPOINT;

var gridWin, chartWin;

nw.Window.open(addr + '/grid.html', {
    width: 1024,
    height: 800
}, function(win) {
    gridWin = win;

    win.on('closed', function(e) {
        win = null;
        chartWin.close(true);
    });
});

nw.Window.open(addr + '/chart.html', {
    width: 1024,
    height: 800,
    show: false
}, function(win) {
    chartWin = win;

    chart_control.window.show = () => win.show();
    win.on('close', function() {
        this.hide();
    });
});

