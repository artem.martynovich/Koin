var http = require('http'),
	finalhandler = require('finalhandler');
	serveStatic = require('serve-static');

var serve = serveStatic("./");
var server = http.createServer(function(req, res) {
	var done = finalhandler(req, res);
	serve(req, res, done);
});
server.listen(9090, () => {
	console.log('listening');
});