# Koin Test App
The app models client-server configuration, however some addresses were hard-coded as `localhost`, and it was only tested on a single machine.

### Server
* Make sure you have MongoDB running available at `localhost:27017`.
* Drop any existing database just to be sure.
* `cd server`
* Install required Node packages: `npm install`
* Review the configuration at `srvcfg.json`
* Start the server: `npm start`

### Client
* Download and install the [latest nwjs](https://nwjs.io/) (the version from NPM won't do).
* Make sure `nw` command is available in the shell
* `cd client`
* Install required Node packages: `npm install`
* Start the client: `npm start`

## Notes
* Selecting a row in Grid View will open Chart View for the given symbol. It is a separate window which can be resized and moved around.
* coinapi doesn't provide a Trades list snapshot, so you have to wait for a while for Trades table to fill with new trades.
* Grid View is updated once in a second by default, Chart View is updated once in ten seconds.
* The only available chart resolution is 1 hour. It doesn't receive updates for now.
* Chart input box is searchable. Selecting a symbol there will load asks, bids and trades for this symbol as if it was selected in Grid View.
* Both views will reconnect to the server if it goes down for a while. They will continue working once the server is up.
